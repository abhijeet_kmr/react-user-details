import React, { Component } from "react";

export default class UserDetails extends Component {
  //   constructor(props) {
  //     super(props);
  //   }
  render() {
    return (
      <div className="user-details">
        <p className="name">{this.props.name}</p>
        <hr />
        <p>Address: {this.props.city},</p>
        <p> {this.props.street}.</p>
        <p> Zip:- {this.props.zipcode}</p>
        <p> Phone:- {this.props.phone}</p>
        <p> Email:- {this.props.email}</p>
      </div>
    );
  }
}
