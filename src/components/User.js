import React, { Component } from "react";
// import Photos from "./Photos";
import UserDetails from "./UserDetails";

export default class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      photos: [],
      posts: [],
    };
  }

  componentDidMount() {
    const id = this.props.data.id;
    fetch(`https://jsonplaceholder.typicode.com/albums?userId=${id}`)
      .then((res) => res.json())
      .then((res) => {
        return res;
      })
      .then((res) => {
        fetch(
          `https://jsonplaceholder.typicode.com/photos?albumId=${res[0].id}`
        )
          .then((photos) => photos.json())
          .then((photos) => {
            this.setState({ photos: photos.slice(0, 5) });
          });
      });

    fetch(`https://jsonplaceholder.typicode.com/posts?userId=${id}`)
      .then((res) => res.json())
      .then((res) => this.setState({ posts: res }));
  }

  render() {
    return (
      <div className="main-container">
        <UserDetails
          name={this.props.data.name}
          city={this.props.data.address.city}
          street={this.props.data.address.street}
          zipcode={this.props.data.address.zipcode}
          phone={this.props.data.phone}
          email={this.props.data.email}
        />

        <div className="user-photos">
          {this.state.photos.map((photo) => (
            <img key={photo.id} src={photo.url} alt="photos" />
          ))}
        </div>

        <div className="user-posts">
          {this.state.posts.map((post) => (
            <div className="post" key={post.id}>
              <p>
                <span>#</span> {post.body}
              </p>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
