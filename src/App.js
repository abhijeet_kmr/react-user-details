import "./App.css";
import User from "./components/User";

import React, { Component } from "react";

export default class App extends Component {
  constructor(props) {
    super();
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    fetch(`https://jsonplaceholder.typicode.com/users`)
      .then((res) => res.json())
      .then((res) => {
        this.setState({ data: res });
      });
  }

  render() {
    return this.state.data.map((user) => {
      return <User key={user.id} data={user} />;
    });
  }
}
